import socket
import sys
import time
import select

#############################
#                           #
# Author: Van Manh VO       #
#                           #
#############################

#Variables constantes
PROXY_IP = 'localhost'
PROXY_PORT = 12800

REMOTE_SERVER_IP = 'localhost'
REMOTE_SERVER_PORT = 8080

DELAY = 0.001
MAX_SEGMENT_SIZE = 2048
MAX_CONNECTIONS = 10

FORWARD_TO = (REMOTE_SERVER_IP, REMOTE_SERVER_PORT)

#Classes
class TargetedServerConnection:
    def __init__(self):
        self.connect = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start(self, host, port):
        try:
            self.connect.connect((host, port))
            return self.connect
        except Exception as e:
            print (e)
            return False

class ProxyServer:
    available_sockets = []
    channel = {}

    def __init__(self, host, port):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((host, port))
        self.server.listen(MAX_CONNECTIONS)
        print("Le serveur Proxy ecoute a present sur le port 12800")

    def main_loop(self):
        self.available_sockets.append(self.server)
        while 1:
            time.sleep(DELAY)
            inputready, outputready, exceptready = select.select(self.available_sockets, [], [])
            for self.s in inputready:
                if self.s == self.server:
                    self.on_accept()
                    break

                self.data = self.s.recv(MAX_SEGMENT_SIZE)
                if len(self.data) == 0:
                    self.on_close()
                    break
                else:
                    self.on_recv()
    def on_accept(self):
        forward = TargetedServerConnection().start(FORWARD_TO[0], FORWARD_TO[1])
        clientsock, clientaddr = self.server.accept()
        if forward:
            print (clientaddr, "est connecte")
            self.available_sockets.append(clientsock)
            self.available_sockets.append(forward)
            self.channel[clientsock] = forward
            self.channel[forward] = clientsock
        else:
            print ("Le serveur distant est actuellement indisponible.")
            print ("Fermer la connexion client", clientaddr)
            clientsock.close()

    def on_close(self):
        print (self.s.getpeername(), "est deconnecte")
        
        #Retirer objects depuis available_sockets
        self.available_sockets.remove(self.s)
        self.available_sockets.remove(self.channel[self.s])
        out = self.channel[self.s]
        
        # Fermer la connexion avec le client
        self.channel[out].close()
        
        # Fermer la connexion avec le serveur distant
        self.channel[self.s].close()
        
        # Nettoyer objects du canal
        del self.channel[out]
        del self.channel[self.s]

    def on_recv(self):
        data = self.data
        print (data)
        self.channel[self.s].send(data)

#Principal
if __name__ == '__main__':
        server = ProxyServer(PROXY_IP, PROXY_PORT)
        try:
            server.main_loop()
        except KeyboardInterrupt:
            print ("Ctrl C - Fermeture du serveur Proxy")
            sys.exit(1)