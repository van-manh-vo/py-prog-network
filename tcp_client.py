import socket

#############################
#                           #
# Author: Van Manh VO       #
#                           #
#############################

HOST = 'localhost'
PORT = 12800

connexion_serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion_serveur.connect((HOST, PORT))
print("Connexion etablie avec le serveur distant {}".format(HOST))

msg_a_envoyer = b""
while msg_a_envoyer != b"bye":
    #Attend l'action de l'user et encode son message
    msg_a_envoyer = input("> ")
    msg_a_envoyer = msg_a_envoyer.encode('utf-8') 
    
    # On envoie le message
    connexion_serveur.send(msg_a_envoyer)
    msg_recu = connexion_serveur.recv(2048)
    print(msg_recu.decode('utf-8')) 

print("Fermeture de la connexion. Au revoir!")
connexion_serveur.close()