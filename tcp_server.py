import socket
from threading import Thread 
from socketserver import ThreadingMixIn

#############################
#                           #
# Author: Van Manh VO       #
#                           #
#############################

#Variables constantes
HOST = 'localhost'
PORT = 8080
MAX_CONNECTIONS = 10

connexion_main = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion_main.bind((HOST, PORT))
threads = []
print("Le serveur ecoute a present sur le port {}".format(PORT))

#Thread Pool
class ClientThread(Thread): 
 
    def __init__(self,ip,port): 
        Thread.__init__(self) 
        self.ip = ip 
        self.port = port 
        print ("[*] Nouvelle connexion pour le client " + ip + ":" + str(port))
 
    def run(self): 
        msg_recu = b""
        while msg_recu != b"bye" : 
            msg_recu = connexion_client.recv(2048)
            print(ip + " : " + msg_recu.decode('utf-8'))
            connexion_client.send(b"OK.")

#Principal
while True:
    connexion_main.listen(MAX_CONNECTIONS)
    (connexion_client, (ip,port)) = connexion_main.accept()
    newthread = ClientThread(ip,port)
    newthread.start() 
    threads.append(newthread)

for t in threads: 
    t.join() 

print("Fermeture de la connexion")
connexion_client.close()
connexion_main.close()