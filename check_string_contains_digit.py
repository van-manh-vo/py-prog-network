import re

# # # # # # # # #
#               # 
#  VO Van Manh  #
#               #
# # # # # # # # #

txt = 'mlrg,jmzeefgze5646ohgg5z10015m15'

num_lookup = re.search(r'\d+$', txt)

if(num_lookup is not None):
	print('Ce texte se termine par le chiffre ' + num_lookup.group())
else:
	print('Ce texte ne se termine pas par un chiffre')