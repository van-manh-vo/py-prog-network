import re

# # # # # # # # #
#               # 
#  VO Van Manh  #
#               #
# # # # # # # # #

mails = ['Marie.Dupond@gmail.com','Lucie.Durand@wanadoo.fr',
'Sophie.Parmentier@@gmail.com','Franck.Dupres.gmail.com',
'Pierre.Martin@lip6.fr','Eric.Deschamps@gmail.com']

matchup = re.compile('([^@]+)@([^@]+[(com|fr|jp)])+')

for mail in mails:
	if matchup.match(mail) is not None:
		print('OK > %s est une adresse mail valide' % mail)
	else:
		print('Erreur >>> %s est une adresse mail invalide' % mail)
