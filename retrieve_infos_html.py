import re
import requests
from bs4 import BeautifulSoup

# # # # # # # # #
#               # 
#  VO Van Manh  #
#               #
# # # # # # # # #

r = requests.get('https://www.lip6.fr/recherche/team_membres.php?acronyme=NPA')
soup = BeautifulSoup(r.content, "html.parser")

person_keys = []
person_names = []
person_tels = soup.find_all('td', attrs={'class':'tel'})

for link in soup.find_all('a'):
    if '../actualite/personnes-fiche.php?ident=' in link.get('href'):
        person_keys.append(link.get('href'))

for (person_key, person_tel) in zip(person_keys, person_tels):
    print(soup.find('a',attrs={'href':person_key}).get_text() + ' - Tel: ' + person_tel.get_text())